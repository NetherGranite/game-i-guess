_ = {}

_.registry  = {}

function _:add(alias, path)
	self.registry[alias] = path
end

function _:parse(map, dir)
	if not dir then dir = "" else dir = dir.."/" end
	for k, v in pairs(map) do
		local keyType = type(v)
		if keyType == "string" then
			self:add(v, dir..v)
		elseif keyType == "table" then
			self:parse(v, dir..k)
		else
			error("attempt to map value of type "..keyType.." to Dependency Service")
		end
	end
end

function _:getPathTo(alias)
	return assert(self.registry[alias], "attempt to getPathTo '"..alias.."', an unregistered file")
end

function _:loadfile(...)
	local aliases = {...}
	local results = {}
	for i = 1, #aliases do
		local alias = aliases[i]
		table.insert(results, loadfile(assert(self.registry[alias], "attempt to loadfile '"..alias.."', an unregistered file")))
	end
	return results
end

function _:dofile(...)
	local aliases = {...}
	local results = {}
	for i = 1, #aliases do
		local alias = aliases[i]
		table.insert(results, dofile(assert(self.registry[alias], "attempt to dofile '"..alias.."', an unregistered file")))
	end
	return results
end

function _:require(...)
	local names = {...}
	local packages = {}
	for i = 1, #names do
		local name = names[i]
		local path = self.registry[name..".lua"]
		assert(path, "attempt to require '"..name.."', an unregistered file")
		table.insert(packages, require(path:gsub("/", "."):sub(1, #path-4)))
	end
	return unpack(packages)
end

return _
