local Color4 = require("DependencyService"):require("Group").create(
	"Color4",
	{"R", "G", "B", "A"},
	{"number", "number", "number", "number"},
	{0, 0, 0, 255}
)

function Color4.blend(c1, p, c2)
	p = p or .5
	return c1*p + c2*(1-p)
end

function Color4.brighten(c, p)
	return Color4.blend(c, p or .5, Color4.white)
end

function Color4.darken(c, p)
	return Color4.blend(c, p or .5, Color4.black)
end

local ops = {"blend", "brighten", "darken"}

local internalConstructor = Color4.new

function Color4.new(...)

	local t
	local wrapper = {...}
	if (#wrapper == 1 and type(wrapper[1]) ~= "table") or #wrapper == 2 then
		local val = wrapper[1]
		t = internalConstructor(val, val, val, wrapper[2] or 255)
	else
		t = internalConstructor(...)
	end

	for i = 1, #ops do
		local op = ops[i]
		t[op] = function(self, ...)
			self:map(Color4[op](self, ...))
		end
	end

return t end

Color4.black = Color4.new(0)
Color4.darkgrey = Color4.new(169)
Color4.grey = Color4.new(192)
Color4.lightgrey = Color4.new(211)
Color4.white = Color4.new(255)

Color4.red = Color4.new(255, 0, 0)
Color4.green = Color4.new(0, 255, 0)
Color4.blue = Color4.new(0, 0, 255)
Color4.cyan = Color4.new(0, 255, 255)
Color4.magenta = Color4.new(255, 0, 255)
Color4.yellow = Color4.new(255, 255, 0)
Color4.pink = Color4.new(255, 192, 203)
Color4.orange = Color4.new(255, 165, 0)

return Color4
