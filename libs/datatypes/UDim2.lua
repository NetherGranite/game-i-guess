local DS = require("DependencyService")
local UDim = DS:require "UDim"
local UDim2 = DS:require("Group").create(
	"UDim2",
	{"X", "Y"},
	{"UDim", "UDim"},
	{UDim.new(), UDim.new()}
)

local internalConstructor = UDim2.new

function UDim2.new(...) local t = {}

	local wrapper = {...}
	local args = (#wrapper == 1 and type(wrapper[1]) == "table") and wrapper[1] or wrapper
	if #args == 2 then
		for i = 1, 2 do
			if type(args[i]) ~= "table" then
				error("bad argument #"..i.." to UDim2.new: expected UDim, got "..type(args[i]))
			elseif not args[i].type then
				error("bad argument #"..i.." to UDim2.new: expected UDim, got table")
			elseif args[i].type ~= "UDim" then
				error("bad argument #"..i.." to UDim2.new: expected UDim, got "..args[i].type)
			end
		end
		t = internalConstructor(args[1], args[2])
	elseif #args == 4 then
		for i = 1, 4 do
			assert(type(args[i]) == "number", "bad argument #"..i.." to UDim2.new: exptected number, got "..type(args[i]))
		end
		t = internalConstructor(UDim.new(args[1], args[2]), UDim.new(args[3], args[4]))
	else
		error("bad input to UDim2.new: expected 2 or 4 arguments, got "..#args)
	end
	
	t.type = "UDim2"

return t end

return UDim2
