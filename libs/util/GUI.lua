local DS = require("DependencyService")
local Vector2, Color4, Event = DS:require("Vector2", "Color4", "Event")

GUI = {}

GUI.elementsInOrder = {}

function GUI:updateElementsInOrder()
	self.elementsInOrder = self.grandparent:getDescendants()
end

function GUI:getElementsInOrder()
	return self.elementsInOrder
end

function GUI:getElementAt(pos)
	local elements = self:getElementsInOrder()
	for i = #elements, 1, -1 do
		local element = elements[i]
		if element:contains(pos) then
			return element
		end
	end
end

function GUI:update()
	for i, element in ipairs(self:getElementsInOrder()) do
		element:updateAbsPos()
		element:updateAbsSize()
	end
end

function GUI:draw()
	for i, element in ipairs(self:getElementsInOrder()) do
		element:draw()
	end
end

function GUI:mousepressed(x, y, button)
	self:mouseaction(x, y, button, "down")
end

function GUI:mousereleased(x, y, button)
	self:mouseaction(x, y, button, "up")
end

GUI.lastDowns = {}

function GUI:mouseaction(x, y, button, direction)
	local element = self:getElementAt(Vector2.new(x, y))
	if element then
		element["mouse"..button..direction]:fire()
		if direction == "down" then
			self.lastDowns[button] = element
		elseif direction == "up" then
			if self.lastDowns[button] == element then
				element["mouse"..button.."click"]:fire()
			end
		end
	end
	if direction == "up" then
		self.lastDowns[button] = nil
	end
end

function GUI:mousemoved(x, y)
	local element = self:getElementAt(Vector2.new(x, y))
	if element ~= GUI.lastOver then
		if GUI.lastOver then
			GUI.lastOver.mouseout:fire()
		end
		if element then
			element.mousein:fire()
		end
	end
	GUI.lastOver = element
end

GUI.Superlative = {}

function GUI.Superlative.new()
	local t = {}

	t.abspos = Vector2.new()
	t.abssize = Vector2.new()

	t.children = {}

	function t:getDescendants()
		local d = {}
		for i = 1, #self.children do
			table.insert(d, self.children[i])
			local inner_d = self.children[i]:getDescendants()
			for j = 1, #inner_d do
				table.insert(d, inner_d[j])
			end
		end
		return d
	end

	function t:contains(point)
		return
			point.X >= self.abspos.X and
			point.X <= self.abspos.X + self.abssize.X and
			point.Y >= self.abspos.Y and
			point.Y <= self.abspos.Y + self.abssize.Y
	end

	t.type = "GUI.Superlative"

	return t
end

GUI.grandparent = GUI.Superlative.new()
GUI.grandparent.abspos = Vector2.new(0, 0)
GUI.grandparent.abssize = Vector2.new(love.graphics.getDimensions())

GUI.Component = {}

function GUI.Component.new(pos, size)
	local t = GUI.Superlative.new()

	t.pos = pos
	t.size = size

	t.parent = GUI.grandparent
	table.insert(GUI.grandparent.children, t)
	GUI:updateElementsInOrder()

	function t:updateAbsPos()
		self.abspos.X = self.pos.X.offset + self.pos.X.scale * self.parent.abssize.X
		self.abspos.Y = self.pos.Y.offset + self.pos.Y.scale * self.parent.abssize.Y
	end

	function t:updateAbsSize()
		self.abssize.X = self.size.X.offset + self.size.X.scale * self.parent.abssize.X
		self.abssize.Y = self.size.Y.offset + self.size.Y.scale * self.parent.abssize.Y
	end

	function t:setParent(parent)
		for i, v in ipairs(self.parent.children) do
			if self.parent.children[i] == self then
				table.remove(self.parent.children, i)
			end
		end
		table.insert(parent.children, self)
		self.parent = parent
		GUI:updateElementsInOrder()
	end

	t.type = "GUI.Component"

	return t
end

GUI.Basic = {}

function GUI.Basic.new(pos, size, backgroundColor, text, textColor, textAlign)
	local t = GUI.Component.new(pos, size)

	t.backgroundColor = backgroundColor or Color4.new()
	t.text = text or ""
	t.textColor = textColor or Color4.new()
	t.textAlign = textAlign or ""

	t.mouse1down = Event.new()
	t.mouse2down = Event.new()
	t.mouse3down = Event.new()
	t.mouse1up = Event.new()
	t.mouse2up = Event.new()
	t.mouse3up = Event.new()
	t.mouse1click = Event.new()
	t.mouse2click = Event.new()
	t.mouse3click = Event.new()
	t.mousein = Event.new()
	t.mouseout = Event.new()

	function t:onHover(f1, f2)
		self.mousein:connect(f1)
		self.mouseout:connect(f2)
	end

	function t:draw()
		love.graphics.setColor(self.backgroundColor:toArray())
		love.graphics.rectangle(
			"fill",
			self.abspos.X, self.abspos.Y,
			self.abssize.X, self.abssize.Y
		)
		love.graphics.setColor(self.textColor:toArray())
		love.graphics.printf(text, self.abspos.X, self.abspos.Y, self.abssize.X, self.textAlign)
	end

	t.type = "GUI.Basic"

	return t
end

return GUI
