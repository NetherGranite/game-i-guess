local Timer = {}

Timer.queue = {}

function Timer:add(...)
	local args = {...}
	local list = { t = 0, i = 1 }
	for i = 1, #args, 2 do
		table.insert(list, {
			duration = args[i],
			func = args[i+1]
		})
	end
	table.insert(self.queue, list)
end

function Timer:update(dt)
	for i, list in ipairs(self.queue) do
		local item = list[list.i]
		list.t = list.t + dt
		if list.t >= item.duration then
			item.func()
			list.t = list.t - item.duration
			list.i = list.i + 1
			if list.i > #list then
				table.remove(self.queue, i)
			end
		end
	end
end

return Timer
