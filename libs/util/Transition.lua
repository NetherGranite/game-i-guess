local Transition = {}

Transition.thingsToTween = {}

Transition.easings = {
	quadratic = function(t) return t^2 end,
	cubic = function(t) return t^3 end,
	quartic = function(t) return t^4 end,
	quintic = function(t) return t^5 end,
	sinusoidal = function(t) return 1-math.cos(t*math.pi/2) end
}

function Transition.tween(obj, field, start, finish, duration, ease, direction)
	Transition.thingsToTween[#Transition.thingsToTween+1] = {
		obj = obj,
		field = field,
		start = start,
		finish = finish,
		duration = duration,
		t = 0,
		ease = ease,
		direction = direction
	}
end

function Transition.tweenTo(obj, field, finish, duration, ease, direction)
	Transition.tween(obj, field, obj[field], finish, duration, ease, direction)
end

function Transition:update(dt)
	local i = 0
	while i <= #self.thingsToTween - 1 do

		i = i + 1

		local data = self.thingsToTween[i]

		data.t = data.t + dt/data.duration

		if data.t >= 1 then
			table.remove(self.thingsToTween, i)
			data.t = 1
		end

		local ease_func = self.easings[data.ease]
		local t
		if data.direction == "in" then
			t = ease_func(data.t)
		elseif data.direction == "out" then
			t = 1-ease_func(1-data.t)
		elseif data.direction == "inout" then
			if data.t < .5 then
				t = ease_func(2*data.t) / 2
			else
				t = (1-ease_func(1-2*(data.t-.5)))/2 + .5
			end
		elseif data.direction == "outin" then
			if data.t < .5 then
				t = (1-ease_func(1-2*data.t)) /2
			else
				t = ease_func(2*(data.t-.5)) / 2 + .5
			end
		else
			error("invalid ease direction of "..data.direction)
		end

		data.obj[data.field] = data.start + t * (data.finish - data.start)
	end
end

return Transition
