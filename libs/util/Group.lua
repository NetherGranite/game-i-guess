local Transition = require("DependencyService"):require "Transition"

local Group = {} --Group class

Group.classes = {} --all Group classes
Group.tweenList = {} --all Group objects being tweened

--static functions
function Group.dist(a, b)
	local sum = 0
	assert(a.type == b.type, "Attempt to find distance between "..a.type.." and "..b.type)
	for field in a.fields() do
		sum = sum + (b[field] - a[field])^2
	end
	return math.sqrt(sum)
end

function Group.type(item)
	local rawtype = type(item)

	if rawtype == "table" then
		if item.type then
			return item.type
		else
			return "table"
		end
	else
		return rawtype
	end
end

function Group.contextualize(a, b, err)
	if type(a) == "table" and type(b) == "table" then
		if a.isAGroup and b.isAGroup and a.type == b.type then
			return a, b
		elseif ((a.isAGroup or b.isAGroup) and a.isAGroup ~= b.isAGroup) then
			if a.isAGroup then
				return a, a.class.new(b)
			elseif b.isAGroup then
				return b, b.class.new(a)
			else
				error("wut")
			end
		end
	end

	local errmsg = "attempt to Group-contextualize "..Group.type(a).." and "..Group.type(b)
	if err then err(errmsg) else error(errmsg) end
end

--updater for tweening and checks
--1:group 2:field 3:start 4:finish 5:duration 6:t 7:bezier
do
	local function lerp(p0, p1, t)
		return p0 + (p1 - p0) * t
	end
	local function tri(c1, c2, c3, t)
		b1 = lerp(c1, c2, t)
		b2 = lerp(c2, c3, t)
		return lerp(b1, b2, t)
	end
	local function quad(d1, d2, d3, d4, t)
		c1 = lerp(d1, d2, t)
		c2 = lerp(d2, d3, t)
		c3 = lerp(d3, d4, t)
		b1 = lerp(c1, c2, t)
		b2 = lerp(c2, c3, t)
		return lerp(b1, b2, t)
	end
	--
	-- function Group:update(dt)
	-- 	--tween logic
	-- 	local i = 1
	-- 	while i <= #self.tweenList do --use while loop because it will delete objects in tweenList
	-- 		local item = self.tweenList[i]
	-- 		local t = item[6]
	--
	-- 		item[6] = t + dt/item[5] --update t
	--
	-- 		local start, finish, mode = item[3], item[4], item[7] --bezier alias
	-- 		local result
	-- 		if not mode then --if there's no bezier attached
	-- 			result = lerp(start, finish, t)
	-- 		else
	-- 			local c1, c2, c3
	-- 			if mode == "in" then
	-- 				result = tri(start, start, finish, t)
	-- 			elseif mode == "inout" then
	-- 				if t < .5 then
	-- 					result = tri(start, start, (start+finish)/2, t*2)
	-- 				else
	-- 					result = tri((start+finish)/2, finish, finish, (t-.5)*2)
	-- 				end
	-- 			elseif mode == "out" then
	-- 				result = tri(start, finish, finish, t)
	-- 			else
	-- 				error("Attempt to tween with bezier mode \""..mode..'"')
	-- 			end
	-- 		end
	--
	-- 		item[1][item[2]] = result --assign new value
	--
	-- 		--remove once tween is complete
	-- 		if t > 1 then
	-- 			table.remove(self.tweenList, i)
	-- 		else
	-- 			i = i + 1 --only if the current isn't being removed increment i
	-- 		end
	-- 	end
	-- end
end

--nonstatic function bank
Group.funcs = {
	tween__field__to = function(self, field, val, duration, mode, direction) --field tweening
		Transition.tweenTo(
			self,
			field,
			assert(val, "no tweenTo value passed"),
			assert(duration, "no tween duration passed"),
			assert(mode, "no mode passed"),
			assert(direction, "no direction given")
		)
	end,
	tweenTo = function(self, dest, duration, mode, direction) --object tweening
		if type(dest) ~= "table" then --ensure the destination is at least a table
			error("attempt to tween "..self.type.." object to "..type(dest))
		elseif not dest.type then --if a generic table is assigned, wrap it as an object of the same class as self
			dest = self.class.new(dest)
		elseif dest.type ~= self.type then --if a group was assign then check to make sure it's of the same class
			error("attempt to tween "..self.type.." object to "..dest.type.."object")
		end
		for field in self.fields() do
			self:tween__field__to(field, dest[field], duration, mode, direction)
		end
	end,
	d__field = function(self, field, amount)
		self[field] = self[field] + assert(amount, "no delta amount passed")
	end,
	d = function(self, other)
		if type(other) ~= "table" then --ensure the delta is at least a table
			error("attempt to delta "..self.type.." object to "..type(other))
		elseif not other.type then --if a generic table is assigned, wrap it as an object of the same class as self
			other = self.class.new(other)
		elseif other.type ~= self.type then --if a group was assign then check to make sure it's of the same class
			error("attempt to tween "..self.type.." object to "..other.type.."object")
		end
		for field in self.fields() do
			self:d__field(field, other[field])
		end
	end,
	dist = function(self, b)
		return Group.dist(self, b)
	end,
	toArray = function(self)
		local vals = {}
		for field, i in self.fields() do
			vals[i] = self[field]
		end
		return vals
	end,
	unpack = function(self)
		return unpack(self:toArray())
	end,
	clone = function(self)
		return self.class.new(self:toArray())
	end,
	map = function(self, other)
		assert(
			Group.type(self) == Group.type(other),
			"attempt to map "..Group.type(self).." to "..Group.type(other)
		)
		for field in self.fields() do
			self[field] = other[field]
		end
	end
}

--class creator
function Group.create(name, fields, types, defaults)

	--establishment of class
	local class = {}
	table.insert(Group.classes, class)

	--class function bank
	class.funcs = {
		fields = function() --field iterator
			local i = 0
			return function()
				i = i + 1
				local v = fields[i]
				if v then
					return v, i
				end
			end
		end
	}
	for i = 1, #fields do
		local field = fields[i]
		class.funcs["tween"..field.."To"] = function(self, val, duration, mode, direction)
			self:tween__field__to(field, val, duration, mode, direction)
		end
		class.funcs["d"..field] = function(self, amount)
			self:d__field(field, amount)
		end
	end

	--class constructor
	function class.new(...) local t = {}

		--parameter flexibility
		local wrapper = {...}
		local args
		if #wrapper == 1 and type(wrapper[1]) == "table" then
			args = wrapper[1]
		else
			args = wrapper
		end

		--instance variable assignment
		for i = 1, #fields do
			local arg = args[i]
			assert(
				arg == nil or type(arg) == types[i] or type(arg) == "table",
				"attempt to pass argument of type "..((type(arg) == "table" and arg.type) and arg.type or type(arg)).." to "..name.." constructor"
			)
			t[fields[i]] = args[i] or defaults[i]
		end

		--function bank pointers
		for k, func in pairs(Group.funcs) do --Group functions
			t[k] = func
		end
		for k, func in pairs(class.funcs) do --class functions
			t[k] = func
		end

		--pointer to class
		t.class = class

		--type indicator
		t.type = name

		--indicator that this is a Group object
		t.isAGroup = true

	return setmetatable(t, Group.mt) end

	--finally return the class
	return class

end

--metamethod bank
do
	local function specialConcat(t, del)
		local rval = ""
		for i = 1, #t-1 do
			rval = rval..tostring(t[i])..del
		end
		return rval..t[#t]
	end

	Group.mt = {
		__add = function(a, b) return Group.combine(a, "+", b) end,
		__sub = function(a, b) return Group.combine(a, "-", b) end,
		__mul = function(a, b) return Group.combine(a, "*", b) end,
		__div = function(a, b) return Group.combine(a, "/", b) end,
		__mod = function(a, b) return Group.combine(a, "%", b) end,

		__tostring = function(t)
			return "("..specialConcat(t:toArray(), ", ")..")"
		end,
		__concat = function(a, b)
			return tostring(a)..tostring(b)
		end
	}
end

--arithmetic handler
do
	local ops = {
		["+"] = function(a, b) return a + b end,
		["-"] = function(a, b) return a - b end,
		["*"] = function(a, b) return a * b end,
		["/"] = function(a, b) return a / b end,
		["%"] = function(a, b) return a % b end
	}

	function Group.combine(a, op, b)
		local typeA, typeB = type(a), type(b)

		--logic
		if typeA == "number" then
			local vals = {}
			for field, i in b.fields() do
				vals[i] = ops[op](a, b[field])
			end
			return b.class.new(vals)
		elseif typeB == "number" then
			local vals = {}
			for field, i in a.fields() do
				vals[i] = ops[op](a[field], b)
			end
			return a.class.new(vals)
		else
			a, b = Group.contextualize(a, b, function() --atempt to contextualize
				error("Attempt to "..op.." "..Group.type(a).." to "..Group.type(b))
			end)

			local vals = {}
			for field, i in a.fields() do
				vals[i] = ops[op](a[field], b[field])
			end
			return a.class.new(vals)
		end
	end
end

return Group
