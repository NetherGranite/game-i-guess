local _ = {}

_.new = function()
	local t = {}

	t.callbacks = {}

	function t:connect(func)
		table.insert(self.callbacks, func)
		return {
			disconnect = function()
				for i = 1, #t.callbacks do
					if t.callbacks[i] == func then
						table.remove(t.callbacks, i)
					end
				end
			end
		}
	end

	function t:fire()
		for i = 1, #self.callbacks do
			self.callbacks[i]()
		end
	end

	t.type = "Event"

	return t
end

return _
