function love.load()

	DS = require "DependencyService"
	DS:parse {
		"main.lua",
		"DependencyService.lua",
		libs = {
			datatypes = {
				"Color4.lua",
				"Vector2.lua",
				"UDim.lua",
				"UDim2.lua"
			},
			util = {
				"Event.lua",
				"Group.lua",
				"GUI.lua",
				"Timer.lua",
				"Transition.lua"
			}
		}
	}

	Group, Vector2, Color4, UDim, UDim2, GUI, Timer, Transition = DS:require("Group", "Vector2", "Color4", "UDim", "UDim2", "GUI", "Timer", "Transition")

	parent = GUI.Basic.new(
		UDim2.new(UDim.new(.5, -200), UDim.new(.5, -100)),
		UDim2.new(UDim.new(0, 400), UDim.new(0, 200)),
		Color4.red,
		"this is a reeeeeeeeeeeeeaaaaaaaaaaaaallllllllllllllllyyyyyyyy nice meme", Color4.blue, "center"
	)

	parent.pos.X:tweenTo({0, 0}, 2, "cubic", "inout")
	parent.backgroundColor:tweenTo(Color4.green, 2, "cubic", "inout")

	parent.mouse3up:connect(function()
		parent.pos.X:tweenscaleTo(1, 1, "cubic", "out")
	end)
	parent.mouse2click:connect(function()
		parent.backgroundColor:tweenBTo(255, .5, "cubic", "in")
	end)
	parent:onHover(function()
		parent.size.X:tweenscaleTo(1, 1, "cubic", "out")
	end, function()
		Transition.tweenTo(parent.size.X, "scale", 0, 1, "quadratic", "in")
	end)

	Timer:add(
		1, function() parent.pos.Y:tweenoffsetTo(0, 1, "cubic", "inout") end,
		1, function() parent.pos.Y:tweenoffsetTo(100, 1, "cubic", "inout") end
	)

end

function love.update(dt)

	Timer:update(dt)
	Transition:update(dt)
	GUI:update(dt)

end

function love.draw()

	GUI:draw()

end

function love.mousepressed(x, y, button, isTouch)

	GUI:mousepressed(x, y, button)

end

function love.mousereleased(x, y, button, isTouch)

	GUI:mousereleased(x, y, button)

end

function love.mousemoved(x, y, dx, dy, istouch)

	GUI:mousemoved(x, y)

end
